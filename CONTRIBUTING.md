# Pour contribuer

Pour contribuer au dépôt :
1. Créer une issue avec le détail de ce qu'on veut faire et le tag qui va bien
2. Créer une branche et une merge request à partir de l'issue
3. Faire sa documentation
4. Mettre à jour le [Changelog](CHANGELOG) avec le numéro de version et la ou les issues corrigées
5. Soumettre la Merge Request
6. Une fois la merge request validée, créer un tag avec la nouvelle version sur la branche `main` et le détail des mises à jour

