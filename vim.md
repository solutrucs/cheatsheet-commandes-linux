# Vim

Vim est un éditeur en ligne de commande qui permet de faire pas mal de choses et de gagner pas mal de temps en connaissant quelques astuces et raccourcis. Voici quelques bases et tips pour débuter.

## Les basiques de Vim

Vim fonctionne sur un système de commandes et de "modes" qui s'activent ou se désactivent avec des raccourcis et des commandes effectuées au clavier. Vous trouverez ci-dessous un certain nombre d'entre-elles.

En cas de doute sur ce que vous avez tapé, ne pas hésiter à user et abuser de la touche `ECHAP` qui permet d'annuler la saisie précédente ou de quitter les modes activés.

### Ouvrir / Fermer un fichier

* Ouvrir un fichier : `vim <chemin vers le fichier>`
* Ouvrir un fichier avec Vim ouvert : `:n <chemin vers le fichier>`
* Fermer le fichier (sans fermer Vim) : `:bd`
* Quitter Vim et fermer le fichier en même temps : `:q`
* Quitter  sans sauvegarder les modifications effectuées sur le fichier : `:q!`

### Sauvegarder le fichier

* Sauvegarder les modifications : `:w`
* Sauvergarder les modifications et quitter Vim : `:x`

### Ecrire dans le fichier

* Passer en mode insertion : `i`
* Passer en mode insertion en rajoutant une ligne avant : `MAJ + o`
* Passer en mode insertion en rajoutant une ligne après : `o`
* Quitter le mode édition : `ECHAP`

* Mettre à jour plusieurs lignes à la fois :
  * Passer en mode visual bloc : `CTRL + v`  
  * Sélectionner les lignes en bougeant le curseur avec les flèches `^` et `v`
  * Passer en mode édition sur les lignes sélectionnées : `MAJ + i`
  * Ecrire le texte que l'on veut rajouter sur les lignes en question
  * Quitter le mode d'édition avec `ECHAP`

### Copier / Coller

* Copier 1 ligne : `yy`
* Copier plusieurs lignes : `x` + `yy` (`x` étant le nombre de lignes à partir du curseur et en dessous)
* Couper 1 ligne : `dd`
* Couper plusieurs lignes : `x` + `yy` (`x` étant le nombre de lignes à partir du curseur et en dessous)
* Coller : `p`

Astuce bonus : vous pouvez user et abuser de la fonction `couper` pour supprimer plus rapidement 1 ou plusieurs lignes dans un fichier

## Tips pour se faciliter la vie avec Vim

### Afficher / Masquer les numéros de ligne

* Afficher les numéros de ligne : `:set number`
* Masquer les numéros de ligne : `:set nonumber`

### Activer / Désactiver la coloration syntaxique

* Activer la coloration syntaxique : `:syntax on`
* Désactiver la coloration syntaxique : `:syntax off`

### Rechercher / Remplacer

La recherche utilise la même syntaxe que la commande `sed`.

* Rechercher sur 1 ligne : `:s /<texte>/`
* Rechercher dans tout le fichier : `:%s /<texte>/`
* Rechercher chaque occurence dans le fichier : `/<texte>`
  * Passer à l'occurence  suivante : `n`
* Rechercher / remplacer sur 1 ligne : `:s /<texte_a_remplacer>/<nouveau_texte>/`
* Rechercher / remplacer dans tout le fichier : `:%s /<texte_a_remplacer>/<nouveau_texte>/g`
* Rechercher / remplacer dans tout le fichier sans prise en compte de la casse : `:%s /<texte_a_remplacer>/<nouveau_texte>/gi`
* Rechercher / remplacer dans tout le fichier avec confirmation : `:%s /<texte_a_remplacer>/<nouveau_texte>/gc`
* Rechercher / remplacer sur certaines lignes : `:x,ys /<texte_a_remplacer>/<nouveau_texte>/g` (en remplaçant `x` et `y` par les numéros de lignes)

### Revenir en arrière ou en avant dans l'historique de Vim

De la même façon qu'un éditeur de texte classique, Vim conserve un historique des actions effectuées dans le fichier, tant que ce dernier reste ouvert dans l'éditeur.

* Annuler la dernière action : `u`
* Remettre l'action que l'on vient d'annuler : `CTRL + R`

### Mettre en pause l'édition d'un fichier

Afin de conserver l'historique des modifications effectuées sur un fichier (voir ci-dessus), on peut "mettre en pause" l'édition d'un fichier pour y revenir après.

* Mettre en pause l'édition : `CTRL + Z`
  * Ceci va revenir au terminal
* Revenir à l'édition du fichier :
  * Afficher la liste des process en pause : `jobs`
  * Revenir au fichier : `fg %x` (remplacer x par le n° du job affiché précédemment
  * S'il y a un petit `+` devant le process concerné, `fg` suffit

### Naviguer rapidement dans un fichier

* Début du fichier : `gg`
* Fin du fichier : `MAJ + g`
* Début de ligne : `^`
* Fin de ligne : `$`

### Panneaux multiples

Les panneaux multiples peuvent servir à plusieurs choses, notamment éditer un fichier en conservant un autre morceau de ce même fichier sous les yeux, ou ouvrir plusieurs fichiers à la fois. A l'ouverture, un panneau affiche le même fichier que celui qui déjà est ouvert et sur lequel le focus est placé.

* Ouvrir un panneau vertical : `CTRL + w` + `v`
* Ouvrir un panneau horizontal : `CTRL + w` + `s`
* Changer le curseur de panneau : `CTRL + w` + `w`

### Ecrire en root un fichier ouvert avec un autre utilisateur

Il peut arriver que l'on édite un fichier en oubliant de passer en `root` ou dans un autre utilisateur. Néanmoins, il est possible de corriger le tir.

* Ecrire les modifications sans fermer et réouvrir le fichier : `:w !sudo tee %`
* Explication détaillée :
  * `:w` : écrire le fichier
  * `!sudo` : on lance un shell avec la commande `sudo`
  * `tee` : on redirige l'output de la commande `vim :w` vers une sortie
  * `%` : le fichier courant
