# Commandes utiles

Memo de commandes utiles à garder sous le coude

## Créer une clé SSH

```
ssh-keygen -t rsa -b 2048 -C "<commentaire>"
```

## Compresser une arborescence dans ses propres archives

```
for i in *; do zip -r "${i%}.zip" "$i"; done
```

## Mettre à jour Ubuntu vers la LTS suivante

Ubuntu met à disposition une commande pour mettre à jour vers la LTS suivante, sans risquer de passer sur une version qui ne serait pas LTS :

```
do-release-upgrade
```
