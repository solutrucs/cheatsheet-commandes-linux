# Cheatsheet Commandes Linux

En cas de doute sur un paramètre, se référer au manuel avec la commande `man <commande>`.

Certaines de ces commandes ne sont pas des commandes par défaut. Il faut donc les installer avec le gestionnaire de packages de la machine avant de pouvoir l'utiliser.

## apt

> Gestionnaire de packages pour Debian/Ubuntu

* `apt update` : mise à jour de la liste des packages disponibles dans les dépôts
* `apt upgrade` : mise à jour des packages
* `apt list --upgradable` : affiche la liste des mises à jour disponibles
* `apt full-upgrade` : mise à jour de l'OS (voir aussi [do-release-upgrade pour Ubuntu](commandes_utiles.md#user-content-mettre-à-jour-ubuntu-vers-la-lts-suivante))
* `apt install <package>` : installe le package `<package>`
* `apt remove <package>` : désinstalle le package `<package>`
* `apt purge <package>` : désinstalle le package `<package>` et supprime tous les fichiers qui lui sont associés
* `apt autoremove` : supprime les packages installés via dépendances et qui ne sont plus utiles
* `apt search <nom>` : recherche les packages dont le nom contient `<nom>`
* `apt list --installed` : liste tous les packages installés

## bat

> Affiche le contenu d'un fichier sans l'ouvrir avec syntax highlighting

* `bat <fichier>` : affiche le contenu du fichier

## cat & zcat

> Affiche le contenu d'un fichier sans l'ouvrir

* `cat <fichier>` : affiche le contenu du fichier
* `zcat <fichier>.gz` : affiche le contenu du fichier compressé aux formats bzip2, gzip, lzip et xz

## cd

> Déplacement dans un dossier

* `cd <chemin>` : déplacement dans un dossier
* `cd -` : revenir au dossier où l'on était précédemment
* `cd ~` ou `cd` : se rendre dans son dossier `HOME`

## chmod

> Change les droits d'accès à un fichier ou un dossier

On peut définir les droits sous forme de chiffres ou de lettres, correspondants aux droits suivants :
* Lecture (`r` / `4`) :
  * lister le contenu d'un dossier
  * accéder au contenu d'un fichier
* Ecriture (`w` / `2`) :
  * créer un fichier dans un dossier
  * modifier le contenu d'un fichier
* Exécution (`x` / `1`) :
  * lister le contenu d'un dossier
  * exécuter le fichier (ex : un script bash)

Pour utiliser la notation par chiffre, il faut spécifier les droits dans l'ordre `utilisateur propriétaire` -> `groupe propriétaire` -> `autres utilisateurs`.
Pour la notation par lettre, les correspondances sont les suivantes :
* `u` : utilisateur propriétaire du fichier ou du dossier
* `g` : groupe propriétaire du fichier ou du dossier
* `o` : autres utilisateurs

Exemples :
* `chmod u+x <fichier>` : donne les droits d'exécution au propriétaire du fichier sans toucher autres autres
* `chmod o-w <dossier>` : retire les droits de lecture d'un dossier aux utilisateurs qui ne sont ni propriétaire ni membre du groupe propriétaire d'un dossier
* `chmod 744 <fichier>` : donne les droits d'exécution au propriétaire du fichier et de lecture aux autres utilisateurs
* `chmod -R g+x <dossier>` : donne les droits d'exécution sur **un dossier et tout son contenu** au groupe propriétaire du dossier

## chown

> Change le propriétaire d'un fichier ou d'un dossier

* `chown <user> <fichier>` : Remplace le propriétaire du fichier par `<user>`
* `chown <user>:<groupe> <fichier>` : Remplace le propriétaire et le groupe du fichier respectivement par `<user>` et `<groupe>`
* `chown -R <user> <dossier>` : Remplace le propriétaire d'**un dossier et tout son contenu** par `<user>`
* `chown -R <user> <lien symbolique>` : Remplace le propriétaire d'**un dossier et tout son contenu** par `<user>`
* `chown -h <user> <lien symbolique>` : Remplace le propriétaire du lien symbolique par `<user>`
* `chown --reference=<chemin vers fichier de référence> <fichier>` : Remplace les propriétaires et groupes propriétaires du fichier par ceux du fichier de référence

## cp

> Copie un dossier ou un fichier vers une nouvelle destination

* `cp <source> <destination>` : copie le fichier vers sa destination
* `cp -R <dossier_source> <dossier_destination>` : copie le dossier vers sa destination

## curl

> Client qui permet d'interagir avec des serveurs HTTP(S), FTP et POP3

* `curl <http://url/fichier.html> --output <fichier.html>` : télécharge le fichier depuis l'url indiquée et l'enregistre avec le nom `<fichier.html>`
* `curl -I <http://url/fichier.html>` : affiche les headers retournées par le serveur
* `curl --user <username>:<password> <http://url/>` : envoie une authentification HTTP au serveur

### curl et les API

`curl` peut également être très pratique pour faire des appels API.

* Envoyer des données :

```
curl <url> \
-u <user>:<password> \
-d param1=valeur1 \
-d param2=valeur2 \
-d param3="valeur 3"
```

* Envoyer des données au format JSON :

```
curl <url> \
-u <user>:<password> \
-H "Content-Type: application/json" \
-d '{"<param1>": "<valeur1>", "<param2>": "<valeur2>"}'
```

* Envoyer des données au format JSON en utilisant un fichier :

Fichier exemple JSON :

```
{
   "param1": "valeur1",
   "param2": "valeur2"
}
```

Appel curl associé :

```
curl -X POST -H "Content-Type: application/json" --data-binary "@<chemin vers le fichier>" <url>
```

## df

> Afficher la liste des disques locaux et distants et l'espace disponible sur ces disques

* `df -h` : permet d'avoir l'espace disque facilement lisible
* `df -i` : permet d'afficher les inodes disponibles au lieu de l'espace disque
* `df -l` : affiche uniquement les disques locaux et pas les volumes distants (ex: NAS)

## diff & sdiff

> Compare 2 fichiers et met en évidence les différences

* `diff <fichier1> <fichier2>` : compare et n'affiche que les différences entre `<fichier1>` et `<fichier2>`
* `sdiff <fichier1> <fichier2>` : compare et affiche les 2 fichiers côte à côte en mettant en évidence les lignes qui divergent

## dpkg

> Utilitaire d'installation, mise à jour et suppression de packages RPM pour Debian/Ubuntu

* `dpkg -i <package>.deb` : installe le package `<package>.deb`
* `dpkg -R --install <dossier>` : installe tous les packages présents dans le dossier `<dossier>`
* `dpkg -r <package>` : désinstalle le package `<package>`
* `dpkg -s <package>` : affiche si le package `<package>` est installé
* `dpkg -L <package>` : affiche les fichiers installés par le package `<package>`
* `dpkg -c <package>.deb` : affiche le contenu du package `<package>`
* `dpkg -l` : liste les packages installés

## du

> Calcule la taille des fichiers dans un dossier

* `du -h` : affiche la liste des fichiers dans le dossier courant et la taille facilement lisible
* `du -s` : affiche la taille prise par le dossier courant
* `du -hs` : affiche la taille totale prise par le dossier courant de façon facilement lisible
* `du -hs <chemin_dossier>` : affiche la taille totale prise par le dossier dont le chemin est spécifié de façon facilement lisible
* `du -ahx <chemin_dossier>` : affiche la taille prise par les dossiers et fichiers dans le dossier indiqué, de façon lisible et uniquement sur le filesystem courant
    * `a` : fichiers et dossiers
    * `h` : facilement lisible
    * `x` : uniquement dans le fs courant

## find

> Rechercher un fichier ou un dossier avec des spécificités ()

* `find <chemin> -name *<chaine>*` : cherche un fichier ou un dossier dont le nom contient `<chaine>`
* `find <chemin> -type f` : cherche un fichier
* `find <chemin> -type d` : cherche un dossier
* `find <chemin> -mtime +x` : cherche les fichiers modifiés de plus de `x` jours
* `find <chemin> -mtime -x` : cherche les fichiers modifiés de moins de `x` jours
* `find <chemin> -atime +x` : cherche les fichiers accédés il y a plus de `x` jours

## grep & zgrep

> Permet de filtrer l'output d'une commande ou de faire une recherche dans un fichier / un dossier

* `cat <fichier> | grep <chaine>` : filtre la sortie de la commande `cat` pour n'afficher que les lignes qui contiennent `<chaine>`
* `grep -r <chaine> <dossier>` : recherche `<chaine>` dans tous les fichiers présents dans `<dossier>`
* `grep <chaine> <fichier>` : recherche `<chaine>` dans `<fichier>`
* `grep -n <chaine> <fichier>` : recherche `<chaine>` dans `<fichier>` et affiche les numéros de ligne
* `grep -i <chaine> <fichier>` : recherche `<chaine>` dans `<fichier>` en ignorant la casse
* `grep -w <chaine> <fichier>` : recherche `<chaine>` dans `<fichier>` uniquement s'il s'agit du mot complet
* `grep -c <chaine> <fichier>` : recherche `<chaine>` dans `<fichier>` et affiche le nombre d'occurences trouvées
* `grep -v <chaine> <fichier>` : recherche `<chaine>` dans `<fichier>` et n'affiche que les lignes qui ne contiennent pas `<chaine>`
* `grep -Ax <chaine> <fichier>` : afficher `x` lignes avant celle qui contient `<chaine>` et la ligne en question
* `grep -Bx <chaine> <fichier>` : afficher `x` lignes après celle qui contient `<chaine>` et la ligne en question
* `grep -Cx <chaine> <fichier>` : afficher `x` lignes avant, la ligne qui contient `<chaine>` et `x` lignes après
* `zgrep <chaine> <fichier>.gz` : fait une recherche dans une archive `gz`

## groupadd & groupdel

> Ajout et suppression d'un groupe

* `groupadd <groupe>` : ajoute le groupe `<groupe>`
* `groupadd -g <gid> <groupe>` : ajoute le groupe `<groupe>` avec le gid `<gid>`
* `groupdel <groupe>` : supprime le groupe `<groupe>`

## head

> Affiche les premières lignes d'un fichier

* `head <fichier>` : affiche les 10 premières lignes d'un fichier
* `head -n x <fichier>` : affiche les `x` premières lignes d'un fichier

## history

> Affiche l'historique des commandes lancées par l'utilisateur pout le shell courant

* `history` : affiche l'historique des commandes lancées par l'utilisateur dans le shell courant

## hostname

> Affiche le nom de la machine

* `hostname` : affiche le nom de la machine courante
* `hostname <nom>` : met à jour le nom de la machine courante

## kill & killall

> Tue un process dont a préalablement récupéré le `pid`

Pour récupérer le pid, voir [ps](#user-content-ps)

* `kill <pid>` : demande la fin du process correspondant au pid indiqué
* `kill -9 <pid>` : force la fin du process correspondant au pid indiqué
* `killall -9 <nom process>` : force la fin de tous les process correspondant au nom indiqué

## ln

> Crée un lien entre une source et une destination

* `ln <fichier ou dossier source> <lien destination>` : créer un hardlink
* `ln -s <fichier ou dossier source> <lien destination>` : crée un lien symbolique entre un fichier ou un dossier et une destination
* `ln -sf <fichier ou dossier source> <lien destination>` : écrase un lien symbolique entre un fichier ou un dossier et une destination

## locate & updatedb

> Rechercher un fichier répertorié dans un index

* `locate <fichier>` : recherche un nom de fichier
* `updatedb` : permet de mettre à jour l'index contenant la liste des fichiers

## ls

> Liste les fichiers présents dans un dossier

* `ls` : affiche les fichiers présents dans le dossier
* `ls -l` : liste les fichiers présents avec les détails (taille, droits, etc...)
* `ls -a` : liste les fichiers présents, y compris les fichiers cachés, et le dossier courant, et parent
* `ls -lh` : liste les fichiers présents avec les détails facilement lisibles (taille, droits, etc...)

## mkdir

> Création de dossier

* `mkdir <nom dossier>` : créer un dossier
* `mkdir -p <nom dossier>/<nom dossier 2>/<nom dossier 3>` : créer un dossier et ses parents s'ils n'existent pas

## mv

> Déplace un dossier ou un fichier vers une nouvelle destination

* `mv <source> <destination>` : déplace le fichier ou le dossier vers la destination
* `mv -f <source> <destination>` : déplace le fichier ou le dossier vers la destination en forçant l'écrasement des fichiers déjà présents

## nc

> Se connecte et écoute sur un port particulier

* `nc -l <port>` : écoute les connexions entrantes sur le port `<port>`
* `nc -v <hôte> <port>` : se connecte à un hôte distant
* `nc -vz <hôte> <port>` : teste la connexion à un hôte distant et ferme la connexion

## netstat

> Liste les sockets réseau ouverts

* `netstat -at` : liste uniquement les connexions TCP
* `netstat -au` : liste uniquement les connexions UDP
* `netstat -patune` : liste tous les sockets ouverts (TCP et UDP), leur état avec pids des process et ports associés

## passwd

> Défini un mot de passe pour un utilisateur

* `passwd <user>` : défini un mot de passe pour `<user>`

## ping

> Envoie un paquet à un hôte distant

Cette commande est utile pour vérifier la connectivité avec un hôte distant. Ne fonctionne pas si ICMP est bloqué sur le réseau.

* `ping <ip>` : envoie un paquet à un hôte distant et affiche les informations relatives à la réponse ou une erreur
* `ping -c <x> <ip>` : envoie un paquet à un hôte distant `x` fois et affiche les informations relatives à la réponse
* `ping <nom dns>` : envoie un paquet à un hôte distant dont le nom est résolu et affiche les informations relatives à la réponse
* `ping -s <ip>` : envoie un paquet à un hôte distant et affiche la taille du payload envoyé

## ps

> Affiche les informations des process en cours

* `ps -ef` : affiche les process et les commandes qui ont permis de les lancer
* `ps aux` : affiche les informations des process en cours
* `ps aux |grep <chaine>` : affiche les informations des process en cours en filtrant la liste sur `<chaine>` (voir [grep](#user-content-grep))

## pwd

> Connaitre le chemin vers le dossier dans lequel on se situe

* `pwd` : affiche le dossier courant
* `pwd -P` : affiche le dossier courant sans prendre en compte les liens logiques

Exemple :
* Arborescence :

```
dossier1/
  dossier2.1/
    dossier3/
      liendossier2.2
  dossier2.2/

```

* Output commande :

```
➜  liendossier2.2 git:(master) pwd
/home/liloumuloup/dossier1/dossier2.1/dossier3/liendossier2.2
➜  liendossier2.2 git:(master) pwd -P
/home/liloumuloup/dossier1/dossier2.2/
```

## rm

> Supprime un fichier ou un dossier

* `rm <fichier>` : supprime un fichier
* `rm -r <dossier>` : supprime un dossier

## rpm

> Utilitaire d'installation, mise à jour et suppression de packages RPM pour RedHat/Centos

* `rpm -iv <package>.rpm` : installe le package `<package>.rpm`
* `rpm -qpR <package>.rpm` : affiche les dépendances du package `<package>.rpm`
* `rpm -ql <package>.rpm` : liste les fichiers installés par le package `<package>.rpm`
* `rpm -qa` : liste tous les packages installés
* `rpm -Uvh <package>.rpm` : met à jour le package `<package>.rpm`
* `rpm -evv <package>` : désinstalle le package `<package>`
* `rpm -qf <fichier>` : affiche le nom du package qui a créé le fichier `<fichier>`

## rsync

> Copier des fichiers et des dossiers, soit entre 2 emplacements sur la même machine, soit entre 2 hôtes, souvent utilisé pour synchronisé des dossiers

* `rsync -v <fichier> <dossier>` : Copie `<fichier>` dans `<dossier>` et affiche l'état de la copie
* `rsync -v <fichier1> <fichier2> <dossier>` : Copie `<fichier1>` et `<fichier2>` dans `<dossier>` et affiche l'état de la copie
* `rsync -av <dossier1> <dossier2>` : Copie `<dossier1>` dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers
* `rsync -av <dossier1> <ip>:<dossier2>` : Copie `<dossier1>` dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers sur une machine distante
* `rsync -av <ip>:<dossier1> <dossier2>` : Copie `<dossier1>` depuis la machine distante dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers
* `rsync -av --progress <ip>:<dossier1> <dossier2>` : Copie `<dossier1>` depuis la machine distante dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers et affiche le progrès
* `rsync -av --dry-run <ip>:<dossier1> <dossier2>` : Simule la copie `<dossier1>` depuis la machine distante dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers et affiche le progrès
* `rsync -av <user>@<ip>:<dossier1> <dossier2>` : Copie `<dossier1>` depuis la machine distante dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers en se connectant avec l'utilisateur `<user>`
* `rsync -av <user>@<ip>:{<dossier1>,<dossier2>} <dossier3>` : Copie `<dossier1>` et `<dossier2>` depuis la machine distante dans le `<dossier2>` en conservant les droits et dates de modifications des fichiers en se connectant avec l'utilisateur `<user>`

## scp

> Transfère des fichiers entre hôtes en utilisant une connexion SSH

Pour les connexions, on peut utiliser les mêmes paramètres que la commande [ssh](#user-content-ssh)

* `scp <fichier local> <hôte>:<fichier distant>` : copie un fichier vers le serveur distant
* `scp <hôte>:<fichier distant> <fichier local>` : copie un fichier depuis le serveur distant
* `scp -r <dossier local> <hôte>:<dossier distant>` : copie un dossier vers le serveur distant
* `scp -3 <hôte1>:<fichier distant> <hôte2>:<fichier distant>` : copie un fichier depuis `<hôte1>` vers `<hôte2>` via hôte local
* `scp -P <port> <fichier local> <hôte>:<fichier distant>` : copie un fichier vers le serveur distant en changeant le port utilisé pour contacter le serveur distant

## ssh

> Connexion à un serveur distant

* `ssh <hôte>` : se connecte au serveur distant avec l'utilisateur courant
* `ssh <user>@<hôte>` : se connecte au serveur distant avec l'utilisateur indiqué
* `ssh -i <chemin vers clé privée> <hôte>` : se connecte au serveur distant en utilisant une clé privée générée précédemment (voir [Créer une clé SSH](commandes_utiles.md#user-content-créer-une-clé-ssh))
* `ssh <hôte> -p <port>` : se connecte au serveur distant avec l'utilisateur courant sur le port indiqué
* `ssh <hôte> <commande>` : exécute une commande sur le serveur distant
* `ssh -L <portsource>:<adresse>:<portdest> -R <portdest>:<adresse>:<portsource>` : permet de créer un pont SSH entre 1 machine distante et la machine locale

Plus d'informations sur les ponts SSH et les redirections de port : [ici](https://newbedev.com/what-s-ssh-port-forwarding-and-what-s-the-difference-between-ssh-local-and-remote-port-forwarding)

## su

> Change d'utilisateur

* `su` : devenir `root`
* `su <user>` : devenir `<user>`
* `su -` : devenir `root` et charger son environnement
* `su - <user>` : devenir `<user>` et charger son environnement
* `su <user> -c "<commande>"` : permet d'exécuter une commande en tant que `<user>`
* `su <user> -s <shell> -c "<commande>"` : permet d'exécuter une commande en tant que `<user>` avec le shell `<shell>`

## sudo

> Permet d'exécuter une commande en tant qu'un autre utilisateur mais sans demander son mot de passe
> Nécessite d'avoir l'autorisation dans la configuration `sudo` de la machine

* `sudo <commande>` : exécute la commande `<commande>` en tant que `root`
* `sudo -u <user> <commande>` : exécute la commande `<commande>` en tant que `<user>`
* `sudo su -` : devenir `root` sans demander le mot de passe `root` (similaire à `sudo -s`)
* `sudo su - <user>` : devenir `<user>` sans demander le mot de passe de l'utilisateur
* `sudo -l` : liste les droits sudo autorisés

## tail

> Affiche les dernières lignes d'un fichier

* `tail <fichier>` : affiche les 10 dernières lignes d'un fichier
* `tail -n x <fichier>` : affiche les `x` dernières lignes d'un fichier
* `tail -f <fichier>` : affiche les 10 dernières lignes d'un fichier et reste en attente d'un éventuelle écriture dans ce fichier (ex: pour un fichier de logs)
* `tail -F <dossier>/*` : tail tous les fichiers présents dans le dossier (utile pour les dossiers ne contenant que des logs par exemple)

## tar

> Crée, compresse, décompresse une archive contenant 1 ou plusieurs fichiers

* `tar cf <archive>.tar <fichier1> <fichier2> <...>` : crée une archive contenant les fichiers `<fichier1>`, `<fichier2>`, ...
* `tar czf <archive>.tar.gz <fichier1> <fichier2> <...>` : crée une archive compressée au format gzip contenant les fichiers `<fichier1>`, `<fichier2>`, ...
* `tar xf <archive>.tar -C <dossier>` : extrait le contenu de l'archive `<archive>.tar` dans le dossier `<dossier>`
* `tar xzf <archive>.tar.gz` : extrait le contenu de l'archive compressée `<archive>.tar.gz`
* `tar caf <archive>.tar.xz <fichier1> <fichier2> <...>` : crée une archive compressée au format indiqué en extension contenant les fichiers `<fichier1>`, `<fichier2>`, ...
* `tar tvf <archive>.tar` : liste le contenu de l'archive `<archive>.tar`

Astuce mémotechnique pour se rappeler des paramètres :
* tar czf -> create ze file / create zip file
* tar xzf -> extract ze file / extract zip file

## top & htop

> Affiche les process en cours et les ressources utilisées par ceux-ci

* `top` : affiche les process en cours et ressources utilisées
* `htop` : idem que `top` avec une interface un peu plus facile à lire et avec des possibilités de trier selon plusieurs critères

## touch

> Crée un fichier vide

* `touch <fichier>` : crée un fichier vide

## useradd & adduser & usermod & userdel

> Ajout et suppression d'un utilisateur

On peut vérifier les informations relatives à un utilisateur dans le fichier `/etc/passwd` ou avec la commande `id`

### useradd

* `useradd <user>` : ajoute l'utilisateur `<user>`
* `useradd -m <user>` : ajoute l'utilisateur `<user>` avec son dossier `HOME`
* `useradd -m -d <chemin> <user>` : ajoute l'utilisateur `<user>` avec son dossier `HOME` dans un chemin différent du chemin par défaut
* `useradd -u <uid> <user>` : ajoute l'utilisateur `<user>` avec un uid spécifique (pratique pour les comptes techniques ou la cohérence entre plusieurs machines)
* `useradd -g <groupe> <user>` : ajoute l'utilisateur `<user>` avec le groupe primaire `<groupe>`
* `useradd -g <groupe> -G <groupe1>,<groupe2> <user>` : ajoute l'utilisateur `<user>` avec le groupe primaire `<groupe>` et les groupes secondaires `<groupe1>` et `<groupe2>`

### adduser

Les paramètres par défaut sont présents dans le fichier `/etc/adduser.conf`.

* `adduser <user>` : ajoute l'utilisateur `<user>` en mode interactif

### usermod

Usermod permet de modifier des attributs d'un utilisateur.

* `usermod -d <chemin> <user>` : modifie le chemin du `HOME` de `user`
* `usermod -d <chemin> -m <user>` : modifie le chemin du `HOME` de `user` et déplace le contenu de l'ancien dossier vers le nouveau
* `usermod -s <chemin vers shell> <user>` : modifie le `SHELL` par défaut
* `usermod -u <uid> <user> : remplace l'uid de `<user>` par `<uid>`
* `usermod -g <groupe> -G <groupe1>,<groupe2> <user>` : modifie le groupe primaire de l'utilisateur `<user>` avec le groupe `<groupe>` et les groupes secondaires `<groupe1>` et `<groupe2>`
* `usermod -G <groupe1>,<groupe2> -a <user>` : modifie les groupes secondaires de l'utilisateur `<user>` avec les `<groupe1>` et `<groupe2>` en conservant les groupes secondaires précédemment configurés

### userdel

* `userdel <user>` : supprime l'utilisateur `<user>`
* `userdel -r <user>` : supprime l'utilisateur `<user>` et ses dossiers `HOME` et `MAIL_DIR`

## wget

> Permet de télécharger des fichiers depuis un serveur FTP ou HTTP(S)

* `wget <http://url/fichier.html>` : télécharge le fichier depuis l'url indiquée
* `wget -O <tutu.html> <http://url/fichier.html>` : télécharge le fichier depuis l'url indiquée et l'enregistre avec le nom `<tutu.html>`
* `wget --mirror --no-parent <http://url/dossier/>` : télécharge l'intégralité des fichiers et dossiers présents à l'url indiquée
* `wget --user=<username> --password=<password> <http://url/fichier.html>` : télécharge le fichier depuis l'url indiquée avec une authentification
* `wget --directory-prefix <chemin> --input-file <liste_urls.txt>` : télécharge tous les fichiers indiqués dans le fichier `<liste_urls.txt>` dans le dossier `<chemin>`.

## yum

> Gestionnaire de packages pour RedHat/Centos

* `yum install <package>` : installe le package `<package>`
* `yum remove <package>` : désinstalle le package `<package>`
* `yum search <package>` : recherche le package `<package>`
* `yum update` : met à jour les packages
* `yum list installed` : liste les packages installés sur la machine
* `yum history` : affiche l'historique des transactions effectuées par `yum`
* `yum undo` : annule une transaction effectuée par `yum`
* `yum redo` : remet une transaction annulée précédemment
* `yum rollback` : annule toutes les transaction jusqu'à une transaction spécifique

## zip & unzip

> Crée, compresse, décompresse une archive au format zip contenant 1 ou plusieurs fichiers

Pour les autres formats, voir [tar](#user-content-tar)

### zip

* `zip <archive>.zip <fichier1> <fichier2> <...>` : crée une archive compressée au format zip contenant les fichiers `<fichier1>`, `<fichier2>`, ...
* `zip -u <archive>.zip <fichier1> <fichier2> <...>` : ajoute les fichiers `<fichier1>`, `<fichier2>`, ... à l'archive compressée au format zip
* `zip -d <archive>.zip <fichier1>` : supprime le fichier `<fichier1>`de l'archive compressée au format zip
* `zip -r <archive>.zip <dossier>` : compresse le dossier `<dossier>` dans l'archive au format zip (fonctionne aussi avec plusieurs dossiers)
* `zipinfo <archive>.zip` : liste les fichiers présents dans l'archive et les informations détaillées sur celle-ci

### unzip

* `unzip <archive>.zip` : décompresse l'archive
* `unzip <archive>.zip -d <chemin>` : décompresse l'archive dans le dossier indiqué
* `unzip -l <archive>.zip` : liste les fichiers présents dans l'archive
