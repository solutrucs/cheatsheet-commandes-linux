# Aide mémoire Linux

Documentation et aides mémoire pour utilisateurs débutants de Linux.

La documentation se divise en plusieurs parties :
* [cheatsheet_commands](cheatsheet_commands.md) : cheatsheet de commandes Linux courantes
* [commandes_utiles](commandes_utiles.md) : liste de commandes pratiques pour gagner du temps
* [tips](tips.md) : astuces pour se faciliter la vie avec un terminal
* [vim](vim.md) : aide à la prise en main pour Vim

Ne pas hésiter à créer des issues s'il manque des choses importantes ou si des points sont erronés !

## Télécharger les PDF générés par la CI

Lors de la création d'un tag, un PDF est généré et déployé en tant que Generic Package dans le dépôt.

Pour les récupérer :
* Packages & registry
* -> Package Registry
* -> cheatsheet-commandes-linux 
* -> récupérer la dernière version des PDF

Une option consiste à passer par l'interface des `tags` :
* Repository
* -> Tags
* -> Cliquer sur le bouton avec une flèche pour télécharger une archive contenant les PDF associés à la version

## Contributeurs
* @Hylobatide
* @liloumuloup - liloumuloup@gmail.com
