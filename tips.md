# Tips

## Naviguer facilement dans un terminal

* Aller à la fin d'une ligne : `CTRL + E`
* Aller au début d'une ligne de commande : `CTRL + A`
* Supprimer le reste de la ligne à partir du curseur : `CTRL + U`

## Mettre en pause / Arrêter une commande

* Mettre en pause une commande en cours : `CTRL + Z`
  * La liste des commandes mises en pause ou en cours : `jobs`
  * Revenir à la précédente commande mise en pause : `fg`
  * Revenir à la commande mise en pause : `fg %x` (remplacer `x` par l'id du process récupéré ci-dessus)
* Arrêter l'exécution d'une commande en cours : `CTRL + C`

## Lancer une commande en arrière-plan

Pour lancer une commande en arrière-plan, il y a 2 méthodes :

### Méthode 1 : Lancer directement la commande en arrière-plan

Pour lancer directement une commande en arrière-plan, il suffit de rajouter `&` à la fin de la ligne.

* Exemple :

```
# On recherche tous les fichiers qui s'appellent "coucou" dans le dossier /
find / -name "coucou" -type f &
```

Il est possible de faire en sorte que la commande puisse continuer de s'exécuter même si l'on ferme sa session. Pour cela, il faut lancer la commande préfixée avec `nohup`.

* Exemple :

```
# On recherche tous les fichiers qui s'appellent "coucou" dans le dossier /
nohup find / -name "coucou" -type f &
```

Attention, l'output de la commande est automatiquement redirigée vers un fichier `nohup.out` situé à l'emplacement où on est au moment où on lance la commande.

### Méthode 2 : Lancer la commande, puis la passer en arrière-plan

  * Lancer sa commande dans le terminal
  * Mettre la commande en pause (voir [Mettre en pause / Arrêter une commande](#user-content-mettre-en-pause-arrêter-une-commande) )
  * Récupérer l'identifiant du job avec `jobs`
  * Passer la commande en arrière-plan avec la commande `bg` (fonctionne sur le même principe que `fg` détaillé ci-dessus)

### Astuces bonus pour les jobs en arrière-plan

* **Supprimer / rediriger l'ouput de la commande** : il est possible que la commande lancée en arrière-plan continue d'afficher son output dans le terminal, ce qui peut être peu pratique. Pour palier à cela, il suffit de rediriger son output :
  * Supprimer l'output : `<commande> > /dev/null 2>&1 &`
  * Rediriger vers un fichier de logs : `<commande> > <chemin vers le fichier de logs>.log 2>&1 &`
* **Vérifier l'état de la commande** : pour vérifier si la commande est toujours en exécution, on peut lancer la commande `jobs` qui va afficher si les jobs sont toujours en cours ou non et afficher les identifiants des jobs en cours
* **Passer de nouveau le job à l'avant-plan** : il suffit de faire le chemin inverse de la méthode 2
  * Récupérer l'identifiant du job avec `jobs`
  * Passer la commande à l'avant-plan avec `fg` (voir [Mettre en pause / Arrêter une commande](#user-content-mettre-en-pause-arrêter-une-commande) pour le détail)
* **Permettre au job de continuer de s'exécuter si on ferme le terminal** : si on ferme le terminal alors qu'on a des jobs en arrière-plan, ceux-ci vont arrêter de s'exécuter. Il est possible de leur permettre de continuer à s'exécuter grâce à `disown` qui fonctionne sur le même principe de `bg` et `fg` (voir [Mettre en pause / Arrêter une commande](#user-content-mettre-en-pause-arrêter-une-commande) )
: 
  * Récupérer l'identifiant du job avec `jobs`
  * Détacher le process de la session : `disown %x` (remplacer `x` par l'id du process récupéré ci-dessus)

## Nettoyer le terminal

Après l'exécution de plusieurs commandes, il est possible que me teminal devienne peu lisible. Pour nettoyer le terminal sans avoir à le fermer / réouvrir, il suffit d'utiliser la commande `clear`.

## Lancer plusieurs commandes à la suite

* Lancer plusieurs commandes à la suite :
```
commande 1 ; commande 2 ; commande 3
```
* Lancer plusieurs commandes à la suite en s'assurant que la précédente a fonctionné correctement :
```
commande 1 && commande 2 && commande 3
```
* Lancer une commande à la suite d'une autre si la précédente a échoué :
```
commande 1 || commande 2 || commande 3
```

## Relancer la commande précédente en sudo

Parfois, on lance une commande nécessitant des droits spécifiques sans mettre le `sudo`. Pour relancer la commande précédente sans avoir à la retaper : 
```
sudo !!
```

## Récupérer son adresse IP publique

```
curl ifconfig.io
```
